
/* --------------------------Codigo de Usuario----------------------- */
package Analizador;

import java_cup.runtime.*;
import java.io.Reader;
import Window.*;

%% //inicio de opciones
   
/* ------ Seccion de opciones y declaraciones de JFlex -------------- */  
   
/* 
    Cambiamos el nombre de la clase del analizador a Lexer
*/
%class AnalizadorLexico

/*
    Activar el contador de lineas, variable yyline
    Activar el contador de columna, variable yycolumn
*/
%line
%column
    
/* 
   Activamos la compatibilidad con Java CUP para analizadores
   sintacticos(parser)
*/
%cup
   
/*
    Declaraciones

    El codigo entre %{  y %} sera copiado integramente en el 
    analizador generado.
*/
%{
    /*  Generamos un java_cup.Symbol para guardar el tipo de token 
        encontrado */
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }
    
    /* Generamos un Symbol para el tipo de token encontrado 
       junto con su valor */
    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }
%}
   

/*
    Macro declaraciones
  
    Declaramos expresiones regulares que despues usaremos en las
    reglas lexicas.
*/
   
/*  Un salto de linea es un \n, \r o \r\n dependiendo del SO   */
Salto = \r|\n|\r\n
   
/* Espacio es un espacio en blanco, tabulador \t, salto de linea 
    o avance de pagina \f, normalmente son ignorados */
Espacio     = {Salto} | [ \t\f]
   
/* Una literal entera es un numero 0 oSystem.out.println("\n*** Generado " + archNombre + "***\n"); un digito del 1 al 9 
    seguido de 0 o mas digitos del 0 al 9 */
Entero = [0-9]*\.?[0-9]+

%% //fin de opciones
/* -------------------- Seccion de reglas lexicas ------------------ */
   
/*
   Esta seccion contiene expresiones regulares y acciones. 
   Las acciones son código en Java que se ejecutara cuando se
   encuentre una entrada valida para la expresion regular correspondiente */
   
   /* YYINITIAL es el estado inicial del analizador lexico al escanear.
      Las expresiones regulares solo serán comparadas si se encuentra
      en ese estado inicial. Es decir, cada vez que se encuentra una 
      coincidencia el scanner vuelve al estado inicial. Por lo cual se ignoran
      estados intermedios.*/
   
<YYINITIAL> {
   
    "if"                { ExecuteWindow.row = "if";                          
                          return symbol(sym.IF); }

    ">"                {  ExecuteWindow.row = ExecuteWindow.row + " > ";                          
                          return symbol(sym.MAYOR); }
    
    "<"                {  ExecuteWindow.row = ExecuteWindow.row + " < ";                          
                          return symbol(sym.MENOR); }
    
    "=="                { ExecuteWindow.row = ExecuteWindow.row + " == ";                          
                          return symbol(sym.IGUAL); }
    
    "!="                { ExecuteWindow.row = ExecuteWindow.row + " != ";                          
                          return symbol(sym.DIFERENTE); }  
                        
    "<="                { ExecuteWindow.row = ExecuteWindow.row + " <= ";                          
                          return symbol(sym.MENOR_IGUAL); } 
 
    ">="                { ExecuteWindow.row = ExecuteWindow.row + " >= ";                          
                          return symbol(sym.MAYOR_IGUAL); }                       
                          
    "("                 { ExecuteWindow.row = ExecuteWindow.row + " ( ";                          
                          return symbol(sym.PARENIZQ); }
    
    ")"                 {  ExecuteWindow.row = ExecuteWindow.row + " ) ";                           
                           return symbol(sym.PARENDER); }

    ";"                 {  return symbol(sym.SEMI); }
    
    {Entero}      {   ExecuteWindow.row = ExecuteWindow.row + yytext();                      
                      return symbol(sym.ENTERO, new Integer(yytext())); }

    /* No hace nada si encuentra el espacio en blanco */
    {Espacio}       { /* ignora el espacio */ } 
}


/* Si el token contenido en la entrada no coincide con ninguna regla
    entonces se marca un token ilegal */
[^]                    {    
                            throw new Error("Caracter ilegal <"+yytext()+">"); 
                        }
